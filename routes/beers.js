const _ = require('lodash')
const Path = require('path-parser')
const { URL } = require('url')
const config = require('../config')
const request = require('request')



//https://gitlab.arinium.fi/arinium/programming-challenge
//http://programming-challenge.arinium.fi/beers/
/*The API serves product images in two different sizes:
Original big sizes served from /images/beers/original/ and /images/energy-drinks/original/. 
So for example "Amigos" beer big size product image would be found at /images/beers/original/amigos.jpg.
Thumbnail small sizes served from /images/beers/thumb/ and /images/energy-drinks/thumb/. So for example
 "Battery No Calories" small size product image would be found at 
 /images/energy-drinks/thumb/battery-no-calories.jpg.
Products may have one image, many images or no image. If a product does not have an image, a 
place holder PNG image with transparent background should be used. Similar to product images, 
the place holder images have two sizes, original & thumb. For energy drinks they can be found 
at /images/energy-drinks/original/no-image.png and /images/energy-drinks/thumb/no-image.png.
*/
/*
request(options, (error, response, body) => {
    if (error) {
        throw new Error(error)
    }
    else {
        response.json(body)
    }
})*/

const options = {
    method: 'GET',
    url: config.BEERS_URL,
    body: ''
}

module.exports = app =>  {
    app.get('/api/beers', (req, res) => {
        request(options, (error, response, body) => {
            if (error) {
                throw new Error(error)
            }
            else {
                res.json(JSON.parse(body))
            }
        })
    }) 
}