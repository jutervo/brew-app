const fs = require('fs')
const http = require('http')
const https = require('https')
var Stream = require('stream').Transform

const config = require('../config')

const thumbUrl = config.BASE_URL + config.BEER_THUMBS
const thumbsFolder =  './client/src/thumbs/'

const originalUrl = config.BASE_URL + config.BEER_ORGINAL
const originalFolder = './original'

const downloadImageToUrl = (url, filename, callback) => {
    var client = http
    if (url.toString().indexOf("https") === 0) {
        client = https
    }
    client.request(url, function(response) {                                        
        var data = new Stream()                                                   
        response.on('data', function(chunk) {                                       
            data.push(chunk)                                                      
        })                                                                         
        response.on('end', function() {                                             
            fs.writeFileSync(filename, data.read())                              
        })                                                                       
   }).on('error', () => {
       console.log('error')
   })
   .end(callback)
}

module.exports = app => {
    app.get('/original/*', (req, res) => {
        console.log(req.query)
        var img = req.params[0] ? req.params[0] : no-image.png
            fs.exists(originalFolder+img, (exists) => {
                if (exists) {
                    res.send(originalFolder+img)
                } else {
                    downloadImageToUrl(originalUrl+img, originalFolder+img, () => {
                        res.send(originalFolder+img)
                    })
                }
            }) 
    })
    app.get('/thumbs/*', (req, res) => {
        console.log(req.query)
        var img = req.params[0] ? req.params[0] : no-image.png
        fs.exists(thumbsFolder+img, (exists) => {
            if (exists) {
                res.send(thumbsFolder+img)
            } else {
                downloadImageToUrl(thumbUrl+img, thumbsFolder+img, () => {
                    res.send(thumbsFolder+img)
            })
        }
        }) 
    })
}

