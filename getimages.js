
const fs = require('fs')
const http = require('http')
const https = require('https')

//https://stackoverflow.com/questions/12740659/downloading-images-with-node-js

var Stream = require('stream').Transform;
var downloadImageToUrl = (url, filename, callback) => {
    var client = http;
    if (url.toString().indexOf("https") === 0){
      client = https;
     }
    client.request(url, function(response) {                                        
      var data = new Stream();                                                    
      response.on('data', function(chunk) {                                       
         data.push(chunk);                                                         
      });                                                                         
      response.on('end', function() {                                             
         fs.writeFileSync(filename, data.read());                               
      });                                                                         
   }).end();
};

downloadImageToUrl('https://www.google.com/images/srpr/logo11w.png', 'public/uploads/users/abc.jpg');
