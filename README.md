# React frontend with simplistic node.js


server
    Node.js + express backend
    gets data from api in json format
    also did some support to download images to server as files. could be stored future in db. not used feature currently. some missing path definitions still in image loading(as it is not used in client)

    runs local:5000

client
    his project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
    simple page with beers and energy-drinks lists. lists can be filtered
    with title characters and sorted with title, preferability or price.
    image loading is done with react-preload-image. scaling of images not ok.
    only thumb images used. 
    bug: default image not loading if image load fails.
    
    runs local:3000

    use npm run dev to run it on local. solution is not deployed to live.
