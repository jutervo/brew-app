const BASE_URL = 'http://programming-challenge.arinium.fi'
const BEERS_URL =   'http://programming-challenge.arinium.fi/beers/'
const ENERGY_URL = 'http://programming-challenge.arinium.fi/energy-drinks/'
const BEER_THUMBS = '/images/beers/thumb/'
const ENERGY_THUMBS = '/images/energy-drinks/thumb/'
const BEERS_ORGINAL = '/images/beers/original/'
const ENERGY_ORGINAL = '/images/energy-drinks/original/'

module.exports = {
    BASE_URL,
    BEERS_URL,
    ENERGY_URL,
    BEER_THUMBS,
    ENERGY_THUMBS,
    BEERS_ORGINAL,
    ENERGY_ORGINAL
}