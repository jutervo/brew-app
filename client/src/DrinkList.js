import React, { Component } from 'react'
import Drink from './Drink'
import _ from 'lodash'

export default class DrinkList extends Component {
    constructor(){
        super()
        this.state = {
            search: '',
            sort: ''
        }
    }
    updateSort(event){
        this.setState({sort: event.target.value})
    }
    
    updateSearch(event){
        this.setState({search: event.target.value.substr(0,20)})
    }
    render() {
        let filteredDrinks = this.props.drinks.filter(
            (drink) => {
                return drink.title.indexOf(this.state.search) !== -1
            }
        )
        if (this.state.sort === 'title') {
            filteredDrinks = _.orderBy(filteredDrinks, this.state.sort)
        }
        else if (this.state.sort === 'price') {
            filteredDrinks = _.sortBy(filteredDrinks,'price-range')
        }
        else if (this.state.sort === 'preferability') {
            filteredDrinks = _.sortBy(filteredDrinks, 'preferability')
        }
        return(
            <div>
                <h2> {this.props.title} </h2>
                <form> Sort by
                <select id="sort" onChange = {this.updateSort.bind(this)} value={this.state.sort}>
                    <option value="nosort"></option>
                    <option value="title">title</option>
                    <option value="price">price</option>
                    <option value="preferability">preferability</option>
                </select>
                </form>
                <ul>
                <li>
                    <label class="id">id </label> 
                    <label class="id">pre </label> 
                    <label class="id">cost </label>
                    <label class="title">title </label> 
                </li>
                    {filteredDrinks.map((drink) => {
                        return <Drink drink={drink} type={this.props.title} key={drink.title} />
                    })}
                </ul>
                <form> Filter by Title chars 
                <input type="text" 
                    value={this.state.search}
                    onChange={this.updateSearch.bind(this)} />
                </form>    
            </div>
        )
    }
}