import React, { Component } from 'react';
import './App.css';

import DrinkList from './DrinkList'
const BEERS_URL =   '/api/beers'
const ENERGY_URL =  '/api/energys'

class App extends Component {

  constructor()
  {
    super();
    this.state = {
      beers: [],
      energy: []
    }
  }
  componentDidMount() {
    fetch(BEERS_URL)
    .then((response) => response.json())
    .then((json) => { 
      this.setState({beers:json})
    })
    fetch(ENERGY_URL)
    .then((response) => response.json())
    .then((json) => { 
      this.setState({energy:json})
    })
  }


  render() {
    return (
      <div>
        <h1> Drinks </h1>
        <DrinkList title="energy-drinks" drinks={this.state.energy}/>
        <DrinkList title="beers" drinks={this.state.beers}/>
        </div>
    )
  }
}

export default App;
