import React, { Component } from 'react'
import PreloadImage from 'react-preload-image'

export default class Drink extends Component {
    // get thumb image
     constructor() {
      super();
      this.state = {
        thumbUrl:'',
        originalUrl:''
      }
    }
    componentDidMount(){
        /*NOT USED
        let img = this.props.drink.images[0] ? this.props.drink.images[0] : 'no-image.png'

        fetch(THUMBS + img, {}) 
        .then((response) => {
            this.setState({thumbUrl:img.replace('-','')} || {thumbUrl:'./thumbs/no-image.png'})
            console.log(response.body)
        })*/
    }

    render() {
        let imgName = this.props.drink.images[0] ? this.props.drink.images[0] : 'no-image.png'
        console.log('img: ' + imgName)
        let thumbs = 'http://programming-challenge.arinium.fi/images/'+this.props.type+'/thumb/' + imgName
       
        return (
            <li>
                <img src={thumbs} height="50" onerror="this.src='./thumbs/no-image.png'"/>
                <label class="id"> {this.props.drink.id} </label> 
                <label class="id"> {this.props.drink.preferability} </label> 
                <label class="id"> {this.props.drink["price-range"]} </label>
                <label class="title"> {this.props.drink.title} </label> {this.props.drink.description} 
            </li>
        )
    }
}