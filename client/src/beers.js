const Beers = [
    {
    'id': 1,
    'title': "Light Beer",
    'images': ["light-beer.jpg" ],
    'description': "Kotimainen alkuperäinen kevytolut. Gluteeniton.",
    'price-range': 30,
    'preferability': 40
    }
    ,
    {
    'id': 2,
    'title': "Fosters",
    'images': [
    "fosters.jpg"
    ],
    'description': "Australialainen tuontiolut.",
    'price-range': 20,
    'preferability': 40
    },
    {
    id: 3,
    title: "Sandels",
    images: [
    "sandels.jpg"
    ],
    description: "Olvin pehmeä olut",
    'price-range': 20,
    preferability: 40
    },
    {
    id: 4,
    title: "Miller Genuine Draft",
    images: [
    "miller.jpg"
    ],
    description: "Amerikkalaisen Millerin perusversio.",
    'price-range': 30,
    preferability: 20
    },
    {
    id: 5,
    title: "Sininen",
    images: [
    "sininen-3.5.jpg"
    ],
    description: "Myös Lahden Sinisenä tunnettu olut. Myydään sekä miedompaa (3,5%) sekä vahvempaa (4,7%) versiota.",
    'price-range': 20,
    preferability: 40
    },
    {
    id: 6,
    title: "Amigos",
    images: [
    "amigos.jpg"
    ],
    description: "Sitruunalla maustettu, hieman simamainen meksikolaistyyppinen raikas olut.",
    'price-range': 30,
    preferability: 30
    },
    {
    id: 7,
    title: "Sol",
    images: [
    "sol-bottle.jpg",
    "sol-can.jpg"
    ],
    description: "Meksikolainen vuodesta 1899 valmistettu olut.",
    'price-range': 30,
    preferability: 10
    },
    {
    id: 8,
    title: "Budéjovicky Budvar",
    images: [
    "budvar.jpg"
    ],
    description: "Tsekkiläinen alkuperäinen 'Budweiser'.",
    'price-range': 40,
    preferability: 30
    },
    {
    id: 9,
    title: "Budweiser",
    images: [
    "budweiser.jpg"
    ],
    description: "Amerikkalainen 'oluiden kuningas'",
    'price-range': 30,
    preferability: 10
    },
    {
    id: 10,
    title: "Coors Light",
    images: [
    "coors-light.jpg"
    ],
    description: "Amerikkalaisen Coors-oluen kevytversio.",
    'price-range': 30,
    preferability: 10
    },
    {
    id: 11,
    title: "Corona Extra",
    images: [
    "corona-extra.jpg"
    ],
    description: "Meksikolainen helpostijuotava olut.",
    'price-range': 30,
    preferability: 20
    },
    {
    id: 12,
    title: "Dos Equis",
    images: [
    "dos-equis.jpg"
    ],
    description: "Vuodesta 1897 valmistettu meksikolainen olut.",
    'price-range': 30,
    preferability: 20
    },
    {
    id: 13,
    title: "Kaiserdom Hefe-Weißbier",
    images: [
    "kaiserdom-hefe-weis.jpg"
    ],
    description: "Saksalainen hedelmäinen vehnäolut.",
    'price-range': 40,
    preferability: 20
    }
]

module.exports = {
    Beers
}